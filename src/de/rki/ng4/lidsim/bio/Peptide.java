/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.bio;

public class Peptide {

	private int gi;
	private String aaSequence;
	
	private String id;
	
	public Peptide(int gi, int part, String aaSequence){
		this.gi = gi;
		this.aaSequence = aaSequence;
		
		this.id = gi + "_" + part;
	}
	
	public String getId(){
		return this.id;
	}
	
	public int getGi(){
		return this.gi;
	}
	
	public String getAaSequence(){
		return this.aaSequence;
	}
	
	public String toString(){
		return this.aaSequence;
	}
}
