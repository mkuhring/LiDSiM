/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.bio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map.Entry;

public class Taxonomy {

	// the relation of gis and taxids is represented by two HashMaps:
	private HashMap<Integer, HashSet<Integer>> giToTaxid;
	private HashMap<Integer, HashSet<Integer>> taxidToGi;

	// the NCBI taxonomy tree is represented by 3 HashMaps.
	// each node (represented by a taxid) in the taxonomy is linked to its 
	// parent (taxidToParent) and to its taxonomic rank (taxidToRank).
	private HashMap<Integer, Integer> taxidToParent;
	private HashMap<Integer, String> taxidToRank;
	// an additional HashMap contains all nodes with a certain taxonomic rank
	private HashMap<String, HashSet<Integer>> rankToTaxids;
	
	// the "active" nodes (taxid with corresponding gis) have 2 additional Maps:
	// taxidToLineage contains the path of node to the root as a list of taxids.
	// each list starts with the root and ends with the node in question.
	private HashMap<Integer,ArrayList<Integer>> taxidToLineage;
	// nodeToDescendants contains a set of all descendants of node (no particular order).
	// it represents a subtree, including the root node as well.
	private HashMap<Integer,HashSet<Integer>> nodeToDescendants;


	public Taxonomy(){
		giToTaxid = new HashMap<Integer, HashSet<Integer>>();
		taxidToGi = new HashMap<Integer, HashSet<Integer>>();

		taxidToParent = new HashMap<Integer, Integer>();
		taxidToRank = new HashMap<Integer, String>();
		rankToTaxids = new HashMap<String, HashSet<Integer>>();

		taxidToLineage = new HashMap<Integer,ArrayList<Integer>>();
		nodeToDescendants = new HashMap<Integer,HashSet<Integer>>();
	}

	public synchronized HashSet<Integer> getTaxids(int gi){
		return(giToTaxid.get(gi));
	}

	public synchronized String getRank(int taxid){
		return(taxidToRank.get(taxid));
	}

	/**
	 * Import GI-TaxID mappings from file, filtered by provided GI set.
	 * 
	 * @param filename Name of the GI-TaxID mapping file
	 * @param gis HashSet of GIs used as filter for the mappings
	 */
	public void parseGiTaxidFile(String filename, HashSet<Integer> gis){

		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(filename)));

			try {
				String line;
				String[] splits;
				Integer gi;
				Integer taxid;
				while ((line = br.readLine()) != null){
					splits = line.split("\t");
					gi = Integer.parseInt(splits[0]);
					taxid = Integer.parseInt(splits[1]);

					if (gis.contains(gi)){
						if (!giToTaxid.containsKey(gi)){
							giToTaxid.put(gi, new HashSet<Integer>());
						}
						giToTaxid.get(gi).add(taxid);

						if (!taxidToGi.containsKey(taxid)){
							taxidToGi.put(taxid, new HashSet<Integer>());
						}
						taxidToGi.get(taxid).add(gi);
					}
				}
			}
			finally {
				br.close();
			}


		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/*
	 * Remove gis which have no corresponding taxid
	 */
	public ArrayList<Integer> removeGisWithNoTaxidMapping(HashSet<Integer> gis){
		ArrayList<Integer> removed = new ArrayList<Integer>();
		for (Iterator<Integer> i = gis.iterator(); i.hasNext();) {
			Integer gi = i.next();
			if (!giToTaxid.containsKey(gi)) {
				removed.add(gi);
				i.remove();
			}
		}
		return(removed);
	}

	/*
	 * Reads a a file with GI to Taxid Mappings (tab-separated "gi\ttaxid")
	 * and prints those lines which gi is in the HashSet gis.
	 */
	public static void filterGiTaxidFile(String input_filename, HashSet<Integer> gis){

		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(input_filename)));

			try {
				String line;
				String[] splits;
				Integer gi;
				while ((line = br.readLine()) != null){
					splits = line.split("\t");
					gi = Integer.parseInt(splits[0]);

					if (gis.contains(gi)){
						System.out.println(line);
					}
				}
			}
			finally {
				br.close();
			}


		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/*
	 * Reads a file with GI to Taxid Mappings (tab-separated "gi\ttaxid")
	 * and writes those lines to another file which gi is in the HashSet gis.
	 */
	public static void filterGiTaxidFile(String input_filename, String output_filename,  HashSet<Integer> gis){

		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(input_filename)));
			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(output_filename)));

			try {
				String line;
				String[] splits;
				Integer gi;

				while ((line = br.readLine()) != null){
					splits = line.split("\t");
					gi = Integer.parseInt(splits[0]);

					if (gis.contains(gi)){
						bw.write(line);
						bw.newLine();
					}
				}
			}
			finally {
				br.close();
				bw.close();
			}


		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	// parse a ncbi taxonomy node file, keep taxid, parent_taxid and rank
	// fills HashMaps with:
	// taxid->parent_taxid
	// taxid->rank
	// example line, note the separater is "\t|\t" and the end of line is "\t|\n"
	// "1	|	1	|	no rank	|		|	8	|	0	|	1	|	0	|	0	|	0	|	0	|	0	|		|"
	public void parseNodes(String filename){

		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(filename)));

			try {
				String line;
				String[] splits;
				Integer taxid;
				Integer parent_taxid;
				String rank;
				while ((line = br.readLine()) != null){
					splits = line.split("\t\\|\t");
					taxid = Integer.parseInt(splits[0]);
					parent_taxid = Integer.parseInt(splits[1]);
					rank = splits[2];

					taxidToParent.put(taxid, parent_taxid);
					taxidToRank.put(taxid, rank);
				}
			}
			finally {
				br.close();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void calcLineage(){
		HashSet<Integer> uniqueTaxids = new HashSet<Integer>(taxidToGi.keySet());

		ArrayList<Integer> newLineage;
		int lastTaxid, nextTaxid;
		String rank;

		for (Integer taxid : uniqueTaxids){
			if (!taxidToLineage.containsKey(taxid)){
				newLineage = new ArrayList<Integer>();
				newLineage.add(taxid);
				lastTaxid = taxid;

				// put own taxid in nodeToDescendants, necessary e.g. when sampling from a branch
				if (!this.nodeToDescendants.containsKey(taxid)){
					this.nodeToDescendants.put(taxid, new HashSet<Integer>());	
				}
				this.nodeToDescendants.get(taxid).add(taxid);

				while (lastTaxid != 1){
					nextTaxid = this.taxidToParent.get(lastTaxid);
					newLineage.add(0, nextTaxid);

					if (!this.nodeToDescendants.containsKey(nextTaxid)){
						this.nodeToDescendants.put(nextTaxid, new HashSet<Integer>());	
						this.nodeToDescendants.get(nextTaxid).add(nextTaxid);
					}
					this.nodeToDescendants.get(nextTaxid).addAll(nodeToDescendants.get(lastTaxid));

					rank = taxidToRank.get(lastTaxid);
					if (!rankToTaxids.containsKey(rank)){
						rankToTaxids.put(rank, new HashSet<Integer>());
					}
					rankToTaxids.get(rank).add(lastTaxid);

					lastTaxid = nextTaxid;
				}

				taxidToLineage.put(taxid, newLineage);
			}
		}
	}

//	public HashMap<Integer, HashSet<Integer>> getTaxidToGi(){
//		return taxidToGi;
//	}

	public synchronized int getLowestCommonAncestor(int taxid1, int taxid2){

//		int taxid1 = giToTaxid.get(gi1);
//		int taxid2 = giToTaxid.get(gi2);

		ArrayList<Integer> lineage1 = taxidToLineage.get(taxid1);
		ArrayList<Integer> lineage2 = taxidToLineage.get(taxid2);

		int maxIndex = Math.min(lineage1.size(), lineage2.size());

		int lca = 1;

		for (int i=0; i<maxIndex; i++){
			if (lineage1.get(i).equals(lineage2.get(i))){
				lca = lineage1.get(i);
			}
			else break;
		}

		return lca;
	}

	public synchronized int getAncestorDistance(int taxid, int ancestorTaxid){

		ArrayList<Integer> lineage = taxidToLineage.get(taxid);

		int distance = lineage.size()-1;

		for (int ancestor : lineage){
			if (ancestor == ancestorTaxid) return distance;
			distance--;
		}

		return 0;
	}

	// for nodes below the rank "species" this function introduces subranks, like "subspecies1", "subspecies2", ...
	// NOTE: subranks are only introduced for nodes with actual data (i.e. gis/proteins)
	// thus, subranks shouldn't used for sampling, because internodes may not have the correct label.
	public void introduceSpeciesSubLevel(){

		HashSet<Integer> uniqueTaxids = new HashSet<Integer>(taxidToGi.keySet());

		rankToTaxids.entrySet();

		ArrayList<Integer> lineage;
		int sublevel;
		String oldrank;
		String newrank;

		for (Integer taxid : uniqueTaxids){
			oldrank = taxidToRank.get(taxid);
			// check if node has no rank
			if (oldrank.matches("no rank")){
				// get lineage of node
				lineage = taxidToLineage.get(taxid);
				// check if node has a species node in lineage 
				// reverse iteration should be faster, because than it goes from leaf to root and the leaf is prob. closer to a species node
				// size()-2, because the last lineage entry is the own node/leaf
				for (int i=lineage.size()-2; i>=0; i--){
					if (taxidToRank.get(lineage.get(i)).matches("species")){
						// if species node found, calculate distance to species node and name new rank
						//						sublevel = lineage.size() - i + 1;
						sublevel = getAncestorDistance(taxid, lineage.get(i));
						if (sublevel == 1){
							newrank = "subspecies";
						}
						else{
							newrank = "subspecies" + sublevel;
						}

						// change rank in taxidToRank 
						taxidToRank.put(taxid, newrank);
						// remove old entry from rankToTaxids
						rankToTaxids.get("no rank").remove(taxid);
						// add new entry to rankToTaxids
						if (!rankToTaxids.containsKey(newrank)){
							rankToTaxids.put(newrank, new HashSet<Integer>());
						}
						rankToTaxids.get(newrank).add(taxid);

						break;
					}
				}
			}
		}
	}

	public HashSet<Integer> taxidsWithRank(String rank){
		if (rank.matches("organism")){
			return new HashSet<Integer>(taxidToGi.keySet());
		}
		return rankToTaxids.get(rank);
	}

	/**
	 * 
	 * 
	 * @param taxid
	 * @return
	 */
	public HashSet<Integer> getDescendants(int taxid){
		return nodeToDescendants.get(taxid);
	}
	
	public HashSet<Integer> getDescendantsWithRank(int taxid, String rank){
		if (rank.matches("organism")){
			return getDescendantLeafs(taxid);
		}
				
		HashSet<Integer> descendants = new HashSet<Integer>();
		
		for (int descendant : getDescendants(taxid)){
			if (taxidToRank.get(descendant).matches(rank)){
				descendants.add(descendant);
			}
		}
		
		return descendants;
	}
	
	public HashSet<Integer> getDescendantLeafs(int taxid){
		HashSet<Integer> leafs = new HashSet<Integer>();
		
		for (int descendant : getDescendants(taxid)){
			if (getDescendants(descendant).size() == 1){
				leafs.add(descendant);
			}
		}
		
		return leafs;
	}

	//	/**
	//	 * Returns the set of taxids (unique) corresponding to a given list of GIs.
	//	 * 
	//	 * @param gis A list of GIs
	//	 * @return HashSet of Taxids
	//	 */
	//	public HashSet<Integer> getTaxids(HashSet<Integer> gis){
	//		HashSet<Integer> taxids = new HashSet<Integer>();
	//		for (int gi : gis){
	//			taxids.add(giToTaxid.get(gi));
	//		}
	//		return taxids;
	//	}

	public HashSet<Integer> getAllTaxids(){
		return new HashSet<Integer>(taxidToGi.keySet());
	}

	public HashSet<Integer> getGis(int taxid){
		return new HashSet<Integer>(taxidToGi.get(taxid));
	}

	public HashSet<Integer> getGis(HashSet<Integer> taxids){
		HashSet<Integer> gis = new HashSet<Integer>();
		for (int taxid : taxids){
			if (taxidToGi.containsKey(taxid)){
				gis.addAll(taxidToGi.get(taxid));
			}
		}
		return gis;
	}

	public ArrayList<String> getRanks() {
		return new ArrayList<String>(rankToTaxids.keySet());
	}

	// assign proteins of a non-leaf node to all of its leafs
	public void reassignProteins(){
		HashSet<Integer> subnodes;
		int taxid;
		Iterator<Entry<Integer, HashSet<Integer>>> it = taxidToGi.entrySet().iterator();
		while (it.hasNext()) {
			taxid = it.next().getKey();
			subnodes = nodeToDescendants.get(taxid);
			// if node has children
			if (subnodes.size() > 1){
				for (int subnode : subnodes){
					// assign proteins to children, if they are a leaf
					if (nodeToDescendants.get(subnode).size() == 1){
						taxidToGi.get(subnode).addAll(taxidToGi.get(taxid));
						for (int gi : taxidToGi.get(taxid)){
							giToTaxid.get(gi).add(subnode);
						}
					}
				}
				// deactivate node
				for (int gi : taxidToGi.get(taxid)){
					giToTaxid.get(gi).remove(taxid);
				}
				it.remove();
			}
		}
	}

	public int countTaxids() {
		return taxidToGi.size();
	}

	public void addTaxid(int originTaxid) {
		taxidToGi.put(originTaxid, new HashSet<Integer>());
	}

	// adds a dummy gi taxid mapping for the genome translation without annotations
	// additionally a lineage is added for taxid 1 (i.e. only 1)
	public void addGenomeDummyGI() {
		int gi = -1;
		int taxid = 1;
				
		if (!giToTaxid.containsKey(gi)){
			giToTaxid.put(gi, new HashSet<Integer>());			
		}
		giToTaxid.get(gi).add(taxid);
		
		ArrayList<Integer> newLineage = new ArrayList<Integer>();
		newLineage.add(taxid);
		taxidToLineage.put(taxid, newLineage);
	}
}
