/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.bio;

import java.io.IOException;
import java.util.HashSet;

import de.rki.ng4.lidsim.io.LineAccessFile;

public class Protein {

	private int gi;
	
	private HashSet<Integer> taxids;
	
	private StringBuffer aaBuffer;
	private String aaSequence;
	
	private LineAccessFile file;
	private int firstLine, lastLine;
	
	private String mode = "none";
	
	public Protein(int gi, LineAccessFile file, int firstLine){
		this.gi = gi;
		this.file = file;
		this.firstLine = firstLine;
		mode = "file";
	}
	
	public void setLastLine(int lastLine){
		this.lastLine = lastLine;
	}
	
	/**
	 * Create a new Protein with a GI and an amino acid sequence.
	 * 
	 * @param gi GI
	 * @param aa amindo acid sequence
	 */
	public Protein(int gi, String aa){
		this.gi = gi;
		this.aaBuffer = new StringBuffer(aa);
	}
	
	/**
	 * Create a new Protein with a GI but with  an amino acid sequence.
	 * The sequence can be provide line by line per appendAaBuffer(String aa).
	 * 
	 * @param gi GI
	 */
	public Protein(int gi){
		this.gi = gi;
		this.aaBuffer = new StringBuffer();
	}
	
	/**
	 * Append an amino acid sequence to the StringBuffer of the protein.
	 * E.g. when read from a Fasta file line by line.
	 * 
	 * @param aa amino acid sequence.
	 */
	public void appendAaBuffer(String aa){
		this.aaBuffer.append(aa);
	}
	
	/**
	 * Returns the protein's GI
	 * 
	 * @return gi
	 */
	public int getGi(){
		return this.gi;
	}
	
	/**
	 * Returns the protein's amino acid sequence.
	 * 
	 * @return amino acid sequence as String
	 */
	public String getAaSequence(){
		if (mode.matches("file")){
			try {
				return file.mergeLines(firstLine, lastLine).toUpperCase().replace('I', 'L');
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return this.aaSequence;
	}
	
	/**
	 * Returns a string representation of the protein, i.e. the amino acid
	 * sequence.
	 */
	public String toString(){
		return this.gi + getAaSequence();
	}
	
	/**
	 * Replace all occurrences of an amino acid with another
	 * 
	 * @param oldAa old amino acid
	 * @param newAa new amino acid
	 */
	public void replaceAA(char oldAa, char newAa){
		aaSequence = aaSequence.replace(oldAa, newAa);
	}

	/**
	 * Finish a currently created protein. I.e. after providing several amino
	 * acid sequences to the protein (e.g. the corresponding lines in a fasta 
	 * file) the amino acid buffer is transfered into a normal String.
	 * 
	 * NOTE: The amino acid sequence buffer will be delete. Do not use
	 * appendAaBuffer(String aa) anymore!
	 */
	public void complete() {
		aaSequence = aaBuffer.toString().toUpperCase();
		aaBuffer = null;
	}
	
	public void setTaxids(HashSet<Integer> taxids){
		this.taxids = taxids;
	}
	
	public HashSet<Integer> getTaxids(){
		return taxids;
	}
}
