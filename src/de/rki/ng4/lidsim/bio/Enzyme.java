/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.bio;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class Enzyme {

	// source: http://stackoverflow.com/questions/1849071/does-a-regular-expression-exist-for-enzymatic-cleavage
	private final static Pattern trypsin = Pattern.compile("(?!P)(?<=[RK])");

	private static int minLength = 8;
	private static int maxLength = 35;
	
	private static int proteinCount = 0;
	private static int splitCount = 0;
	private static int peptideCount = 0;

	public static ArrayList<Peptide> digest(Protein protein){
		ArrayList<Peptide> peptides = new ArrayList<Peptide>();

		String[] splits = trypsin.split(protein.getAaSequence());

		int counter = 1;
		for (String split : splits){
			if (minLength <= split.length() && split.length() <= maxLength){
				peptides.add(new Peptide(protein.getGi(), counter++, split));
			}
		}
		
		proteinCount++;
		splitCount = splitCount + splits.length;
		peptideCount = peptideCount + peptides.size();

		return peptides;
	}

	public static ArrayList<ArrayList<Peptide>> digest(ArrayList<Protein> proteins){
		ArrayList<ArrayList<Peptide>> peptides = new ArrayList<ArrayList<Peptide>>();

		for (Protein protein : proteins){
			peptides.add(new ArrayList<Peptide>(digest(protein)));
		}

		return peptides;
	}

	public static void setMinLength(int newMinLength){
		minLength = newMinLength;
	}

	public static void setMaxLength(int newMaxLength){
		maxLength = newMaxLength;
	}
	
	public static void printReport(){
		System.out.println(" digested " + proteinCount + " proteins into " + splitCount + " peptides -> " + peptideCount + " after length filter");
	}
}
