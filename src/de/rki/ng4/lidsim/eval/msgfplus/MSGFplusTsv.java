/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.eval.msgfplus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/* Parser for MS-GF+ tsv files
 * 
 * for each line a MSGFplusPSM is created
 */
public class MSGFplusTsv {

	private String filename;
	
	@SuppressWarnings("unused")
	private String header;
	private ArrayList<MSGFplusPSM> psms;
	
	public MSGFplusTsv(String filename){
		this.filename = filename;
		this.psms = new ArrayList<MSGFplusPSM>();
		
		parsePSMs();
	}
	
	private void parsePSMs(){
		try {
			BufferedReader tsvBR = new BufferedReader(new FileReader(new File(this.filename)));

			try {
				String line;
				if ((line = tsvBR.readLine()) != null){
					header = line;
				}
				while ((line = tsvBR.readLine()) != null){				
					psms.add(new MSGFplusPSM(line));
				}
			}
			finally {
				tsvBR.close();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<MSGFplusPSM> getPSMs(){
		return psms;
	}
	
	public HashSet<Integer> getGIs(){
		HashSet<Integer> gis = new HashSet<Integer>();
		for (MSGFplusPSM psm : psms){
			gis.add(psm.getMatchGi());
		}
		return gis;
	}
}
