/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.eval.msgfplus;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class PeptideCleaner {

	// regex to remove modifications and trailing AAs from peptide sequence
	private static final String modRegex = "([+-]\\d+\\.?\\d*)|(^[A-Z]\\.)|(\\.[A-Z]$)";
	private static final Pattern modPattern = Pattern.compile(modRegex);
	private static final String modReplacement = "";

	public static String clean(String peptide) {
		return(modPattern.matcher(peptide).replaceAll(modReplacement));
	}

	// pattern test
	public static void main(String[] args){
		ArrayList<String> tests = new ArrayList<String>();
		tests.add("RAVEGTPFEC+57.021LKDAFVGPTLIAYSMEHPGAAAR");
		tests.add("R.HYAHVDC+57.021PGHADYVK.N");

		ArrayList<String> checks = new ArrayList<String>();
		checks.add("RAVEGTPFECLKDAFVGPTLIAYSMEHPGAAAR");
		checks.add("HYAHVDCPGHADYVK");

		String cleanPeptide;
		for (int i=0; i<tests.size(); i++){
			System.out.println(i + " input: " + tests.get(i).length() + " " + tests.get(i));
			System.out.println(i + " expec: " + checks.get(i).length() + " " + checks.get(i));
			cleanPeptide = clean(tests.get(i));
			System.out.println(i + " outpu: " + cleanPeptide.length() + " " + cleanPeptide);
			System.out.println(i + " match: " + (checks.get(i).length() == cleanPeptide.length()) +
					" " + checks.get(i).matches(cleanPeptide));
		}
	}	
}
