/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.eval.msgfplus;

/*
 * Parses a line in a MS-GF+ tsv file
 */
public class MSGFplusPSM {

	// Header of MSGF+ tsv file:
	// #SpecFile	SpecID	ScanNum	Title	FragMethod	Precursor	IsotopeError	PrecursorError(ppm)	Charge	Peptide	Protein	DeNovoScore	MSGFScore	SpecEValue	EValue	QValue	PepQValue
	
	// what I need now is: Title, Peptide Length, Protein and Protein GI
	private String title;
	private int peptideLength;
	private String protein;
	private int matchGi;

	public MSGFplusPSM(String line){
		String[] splits = line.split("\t");
		title = splits[3];
		peptideLength = PeptideCleaner.clean(splits[9]).length();
		protein = splits[10];
		matchGi = Integer.parseInt(protein.split("\\|")[1]);
	}

	public String getTitle(){
		return title;
	}
	
	public int getMatchGi(){
		return matchGi;
	}
	
	public int getPeptideLength(){
		return peptideLength;
	}
}
