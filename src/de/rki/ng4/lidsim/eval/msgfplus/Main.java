/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.eval.msgfplus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import de.rki.ng4.lidsim.Match;
import de.rki.ng4.lidsim.Result;
import de.rki.ng4.lidsim.Search;
import de.rki.ng4.lidsim.bio.Taxonomy;

public class Main {

	public static void main(String[] args) {

		// parameter
		int index = 0;
		// MSGF+ results
		String msgfFile = args[index++];
		
		// gi-taxid mapping
		String gi_taxid_file = args[index++];
		// taxonomy nodes
		String tax_node_file = args[index++];
		
		// result output
		String result_file =  args[index++];
		
		// spectra origin taxid
		int originTaxid = Integer.parseInt(args[index++]);
		
		// spectra input number
		int inputNumber = Integer.parseInt(args[index++]);
		
		// Import MSGF+ results (tsv)
		// make FDR cutoff before!
		MSGFplusTsv tsv = new MSGFplusTsv(msgfFile);
		ArrayList<MSGFplusPSM> psms = tsv.getPSMs();
		
		System.out.print("imported " + psms.size() + " psms (gis) ");
				
		// Import taxonomy
		System.out.println("gi extraction... ");
		HashSet<Integer> gis = tsv.getGIs();

		System.out.println("gi-taxid import... ");
		Taxonomy tax = new Taxonomy();
		tax.parseGiTaxidFile(gi_taxid_file, gis);
		tax.addTaxid(originTaxid);
		
		int totalTax = tax.countTaxids();
		System.out.println(totalTax + " orgs in total... ");
		
		ArrayList<Integer> removed = tax.removeGisWithNoTaxidMapping(gis);
		if (removed.size() > 0) System.out.print("removed " + removed.size() + " missing gis: " + removed.toString());

		System.out.println("tax nodes import... ");
		tax.parseNodes(tax_node_file);
		
		System.out.println("lineage calculations... ");
		tax.calcLineage();
		tax.introduceSpeciesSubLevel();
		tax.reassignProteins();
		
		// create a Match for every PSM
		System.out.println("parse matches... ");
		HashMap<String, ArrayList<Match>> matches = new HashMap<String, ArrayList<Match>>();
		for (MSGFplusPSM psm : psms){
			for (int taxid : tax.getTaxids(psm.getMatchGi())){
				if (!matches.containsKey(psm.getTitle())){
					matches.put(psm.getTitle(), new ArrayList<Match>());
				}
				matches.get(psm.getTitle()).add(
						new Match(psm.getTitle(), 0, originTaxid, 
								psm.getPeptideLength(), psm.getMatchGi(), taxid));				
			}
		}
		HashMap<Integer, HashMap<String, ArrayList<Match>>> temp = new HashMap<Integer, HashMap<String, ArrayList<Match>>>();
		temp.put(originTaxid, matches);
		Search.checkUniques(temp);
		Search.removeNonuniques(temp, tax);
		
		ArrayList<Match> allMatches = new ArrayList<Match>();
		for (HashMap<String, ArrayList<Match>> orgmatches : temp.values()){
			for (ArrayList<Match> matchList : orgmatches.values()){
				allMatches.addAll(matchList);
			}
		}
		
		// add pseudocount Matches for unmatched spectra
		while (allMatches.size() < inputNumber){
			allMatches.add(new Match("0", 0, 0, 0, 0, 0));
		}
		
		// create Result and export
		System.out.println("export results... ");
		Result results = new Result(result_file, tax);
		results.export(allMatches, 0, originTaxid, "spectra");
		results.close();
	}
}
