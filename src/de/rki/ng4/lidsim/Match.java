/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim;

public class Match {

	private String queryId;
	
	private int queryGi;
	private int queryTaxid;
	private int queryLength;
	
	private int targetGi;
	private int targetTaxid;
	
	private boolean unique;
	
	public Match(String queryId, int queryGi, int queryTaxid, int queryLength, int targetGi, int targetTaxid){
		this.queryId = queryId;
		this.queryGi = queryGi;
		this.queryLength = queryLength;
		this.queryTaxid = queryTaxid;
		this.targetGi = targetGi;
		this.targetTaxid = targetTaxid;
		this.unique = true;
	}
	
	public String getQueryId(){
		return queryId;
	}
	
	public int getQueryGi(){
		return queryGi;
	}
	
	public int getQueryTaxid(){
		return queryTaxid;
	}
	
	public int getQueryLength(){
		return queryLength;
	}
	
	public int getTargetGi(){
		return targetGi;
	}
	
	public int getTargetTaxid(){
		return targetTaxid;
	}

	public void setUnique(boolean isUnique) {
		this.unique = isUnique;
	}
	
	public boolean getUnique(){
		return(unique);
	}
}
