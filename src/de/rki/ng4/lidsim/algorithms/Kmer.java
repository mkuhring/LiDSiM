/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import de.rki.ng4.lidsim.bio.Peptide;
import de.rki.ng4.lidsim.bio.Protein;
import de.rki.ng4.lidsim.bio.Taxonomy;


/* A very simple kmer filter to search Peptides in a List of Proteins 
 * 
 * But, this implementation is very slow and thus not useful!
 */
public class Kmer {

	private HashMap<String, HashSet<Integer>> kmers;
	private int kmerSize;
	
	private Taxonomy tax;
	
	public Kmer(ArrayList<Protein> database, int kmerSize, Taxonomy tax){
		
		this.tax = tax;
		this.kmerSize = kmerSize;
		kmers = new HashMap<String, HashSet<Integer>>();
		
		String kmer;
		String sequence;
		
		for (Protein protein : database){		
			sequence = protein.getAaSequence();
			for (int j=0; j<sequence.length()-kmerSize+1; j++){
				kmer = sequence.substring(j, j+kmerSize);
				
				if (!kmers.containsKey(kmer)){
					kmers.put(kmer, new HashSet<Integer>());
				}
				kmers.get(kmer).addAll(tax.getTaxids(protein.getGi()));
			}
		}	
	}
	
	public HashSet<Integer> search(Peptide peptide){
		
		HashSet<Integer> matches = new HashSet<Integer>();
		String sequence = peptide.getAaSequence();
		String kmer;
		
		for (int j=0; j<sequence.length()-kmerSize; j++){
			kmer = sequence.substring(j, j+kmerSize);
			
			if (kmers.containsKey(kmer)){
				matches.addAll(tax.getGis(kmers.get(kmer)));
			}
		}
		
		return matches;
	}
	
}
