/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.algorithms;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * <p>Implementation of the approximate Boyer-Moore algorithm with k mismatches (i.e. max Hamming distance of k).</p>
 * 
 * <p>Tarhio,J. and Ukkonen,E. (1993) Approximate Boyer-Moore String Matching. SIAM J. Comput., 22, 243�260.</p>
 * 
 * @author Mathias Kuhring
 */
public class ApproximateBoyerMoore {

	private char[] p;

	private HashMap<Character, Integer> ready;
	private HashMap<Integer, HashMap<Character, Integer>> dk;

	private int m, k;

	public ApproximateBoyerMoore(String pattern, int mismatches){

		this.p = pattern.toCharArray();

		ready = new HashMap<Character, Integer>();
		dk = new HashMap<Integer, HashMap<Character, Integer>>();

		m = pattern.length();
		k = mismatches;

		preprocessing();
	}
	
	private void preprocessing(){

		for (int i=m; i >= m-k; i--){
			dk.put(i, new HashMap<Character, Integer>());
		}

		for (char a : p){

			if (!ready.containsKey(a)){
				ready.put(a, m+1);
			}

			for (int i=m; i >= m-k; i--){
				if (!dk.get(i).containsKey(a)){
					dk.get(i).put(a, m);
				}
			}
		}

		// in p[i-1] -1 because Java index starts with 0
		// in max(i+1, m-k) +1 because otherwise j=i -> 0 shift
		for (int i=m-1; i>=1; i--){
			for (int j=ready.get(p[i-1])-1; j >= Math.max(i+1, m-k); j--){
				dk.get(j).put(p[i-1], j-i);
			}
			ready.put(p[i-1], Math.max(i+1, m-k));
		}
	}

	public boolean isIn(String text){
		char[] t = text.toCharArray();
		int n = text.length();

		int j, h, i, neq, d;

		j = m;

		while (j <= n){ // paper says j<=n+k, but then index h is outside of t
			h = j;
			i = m;
			neq = 0;
			d = m-k;

			while (i > 0 && neq <= k){
				if (i >= m-k){
					if (dk.get(i).containsKey(t[h-1])){
						d = Math.min(d, dk.get(i).get(t[h-1]));
					}
				}
				if (t[h-1] != p [i-1]){
					neq++;
				}
				i--;
				h--;
			}
			if (neq <= k){
				return true;
			}
			j = j+d;
		}
		return false;
	}

	public static void main(String[] args){
		test1();
		test2();
	}
	
	private static void test1(){
		String pattern = "abbbb";
		String text = "abaacbbabbba";
		ApproximateBoyerMoore mysearch = new ApproximateBoyerMoore(pattern, 4);
		System.out.println(mysearch.isIn(text));
	}
	
	private static void test2(){
		String text = "MLMKPYISHRAIICTLTLLIIIITVLFTFLRSSDVPEYITAPVRKPGDIENSVLATGRIDAIERVNVGAQVSGQLKSLKVKQ";
		ArrayList<String> pattern = new ArrayList<String>();
		// 0 error
		pattern.add("IITVLFTFLRSSD");
		pattern.add("VRKPGDIENSVLA");
		pattern.add("YISHRAI");
		// 1 error
		pattern.add("IITVLVTFLRSSD");
		pattern.add("VRVPGDIENSVLA");
		pattern.add("YISIRAI");
		// 2 error
		pattern.add("IKTVLFTFLRDSD");
		pattern.add("VRAPGDIENSPLA");
		pattern.add("YIAHRII");
		
		ApproximateBoyerMoore mysearch;
		for (String p : pattern){
			System.out.println("pattern: " + p);
			mysearch = new ApproximateBoyerMoore(p,1);
			System.out.println(mysearch.isIn(text));
		}
	}

}
