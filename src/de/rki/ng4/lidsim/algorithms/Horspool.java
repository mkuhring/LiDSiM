/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.algorithms;

import java.util.Arrays;
import java.util.HashMap;


/**
 * <p>Implementation of the Horspool algorithm for fast exact string matching.</p>
 * 
 * <p>A short description: <a href="https://www.mi.fu-berlin.de/wiki/pub/ABI/AdvancedAlgorithms11_Searching/script-01_02-Intro_HorspoolWuManber.pdf">
 * https://www.mi.fu-berlin.de/wiki/pub/ABI/AdvancedAlgorithms11_Searching/script-01_02-Intro_HorspoolWuManber.pdf</a> (Page 7 and 8).</p>
 * 
 * @author Mathias Kuhring
 */
public class Horspool {

	private char[] p;
	private char[] t;
	
	private int m, n;
	
	private HashMap<Character,Integer> d;
	
	public Horspool(String pattern){
		
		p = pattern.toCharArray();
		m = this.p.length;
		
		d = new HashMap<Character,Integer>();
		for (int j=0; j < m - 1; j++){
			d.put(p[j], m - j - 1);
		}
	}
	
	public void searchIn(String text){
		t = text.toCharArray();
		n = t.length;
		
		int j;
		int pos = 0;
		
		while (pos <= n - m){
			j = m;
			
			while (j > 0 && t[pos+j-1]==p[j-1]){
				j--;
			}
			
			if (j == 0){
				System.out.println(Arrays.toString(p) + " occurs at position " + (pos+1));
			}
			
			if (d.containsKey(t[pos+m-1])){
				pos = pos + d.get(t[pos+m-1]);
			}
			else{
				pos = pos + m;
			}
		}
	}
	
	public static void main(String[] args){
		
		String pattern = "announce";
		String text = "cpmxannounceualxconferencexannounce";
		
		Horspool search = new Horspool(pattern);
		search.searchIn(text);
	}
	
}
