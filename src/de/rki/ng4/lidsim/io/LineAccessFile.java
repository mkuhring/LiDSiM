/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class LineAccessFile extends RandomAccessFile{

	private String fileName;
	private int lineCount;
	private long[] linePointer;

	// counts and indexes the lines of the file
	public LineAccessFile(String fileName) throws IOException{
		super(fileName, "r");
		this.fileName = fileName;
				
		countLines();
		indexLines();
	}
	
	// returns the number of lines
	public int getLineCount(){
		return lineCount;
	}
	
	// reads the single stated line
	public synchronized String readLine(int line) throws IOException{
		this.seek(linePointer[line]);
		return this.readLine();
	}
		
	//reads lines in the range of "from" and "to" and returns them in a list.
	public synchronized ArrayList<String> readLines(int from, int to) throws IOException{
		ArrayList<String> lines = new ArrayList<String>();
		this.seek(linePointer[from]);
		for (int i=0; i<(to-from+1); i++){
			lines.add(this.readLine());
			System.out.println(lines.get(i));
		}
		return lines;
	}
	
	/* reads trimmed lines in the range of "from" and "to" and directly merges 
	 * them into one string. 
	 */
	public synchronized String mergeLines(int from, int to) throws IOException{
		StringBuffer merged = new StringBuffer();
		this.seek(linePointer[from]);
		for (int i=0; i<(to-from+1); i++){
			merged.append(this.readLine().trim());
		}
		System.out.println(merged.toString());
		return merged.toString();
	}

	// index the lines, i.e. which byte offset has each line
	private void indexLines() throws IOException {
		linePointer = new long[lineCount];
		for (int i=0; i<linePointer.length; i++){
			linePointer[i] = this.getFilePointer();
			this.readLine();
		}
	}

	// count the lines in the file
	private void countLines() throws IOException{
		lineCount = 0;
		BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));
		while(br.readLine() != null){
			lineCount++;
		}
		br.close();
	}
	
	// testing
	public static void main(String[] args){
		String filename = "data/ncbi_proteins/bacteria.faa";
		try {
			LineAccessFile file = new LineAccessFile(filename);
			file.readLines(897, 900);
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
