/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Vector;

import de.rki.ng4.lidsim.bio.Protein;

public class Fasta {

	private String filename;

	private ArrayList<String> header;
	private HashSet<Integer> gis;

	private ArrayList<Protein> proteins;
	private boolean loaded = false;

	public Fasta(String filename){
		this.filename = filename;
		this.header = new ArrayList<String>();
		this.gis = new HashSet<Integer>();
		indexFasta();
	}

	// index the Fasta file by saving the headers 
	private void indexFasta(){
		try {
			BufferedReader fasta_br = new BufferedReader(new FileReader(new File(this.filename)));

			try {
				String line;
				while ((line = fasta_br.readLine()) != null){
					if (line.startsWith(">")){
//						this.header.add(line);
						
						gis.add(Integer.parseInt(line.split("\\|")[1]));
					}
				}
			}
			finally {
				fasta_br.close();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returns the set of GIs extracted from the Fasta headers
	 * 
	 * @return HashSet of Integers (GIs)
	 */
	public HashSet<Integer> getGIs(){
		if (this.gis == null){
			extractGIs();
		}
		return this.gis;
	}

	// extract the GIs from the fasta headers
	private void extractGIs(){
		this.gis = new HashSet<Integer>();
		for (String h : header){
			this.gis.add(Integer.parseInt(h.split("\\|")[1]));
		}
	}

	/**
	 * Returns the list of all proteins imported from the Fasta file
	 * 
	 * @return ArrayList of Proteins
	 */
	public ArrayList<Protein> getProteins(){
		if (!loaded){
			loadProteinsToMemory();
		}
		return proteins;
	}

	public void loadProteins(String mode){
		if (mode.matches("memory")){
			loadProteinsToMemory();
		}
		else {
			loadProteinWithIndex();
		}
	}
	
	private void loadProteinWithIndex() {
		Vector<Protein> proteins = new Vector<Protein>();
		
		try {
			LineAccessFile file = new LineAccessFile(this.filename);
			
			String line;
			int gi = 0;
			int lineCount = 0;
			
			while ((line = file.readLine()) != null){
				if (line.startsWith(">")){
					gi = Integer.parseInt(line.split("\\|")[1]);
					if (gis.contains(gi)){
						proteins.add(new Protein(gi, file, lineCount));
						proteins.lastElement().setLastLine(lineCount);
					}
				}
				else{
					if (gis.contains(gi)){
						proteins.lastElement().setLastLine(lineCount);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadProteinsToMemory(){
		if (!loaded){
			try {
				Vector<Protein> proteins = new Vector<Protein>();

				BufferedReader fasta_br = new BufferedReader(new FileReader(new File(this.filename)));

				try {
					String line;
					int gi = 0;

					while ((line = fasta_br.readLine()) != null){
						if (line.startsWith(">")){
							gi = Integer.parseInt(line.split("\\|")[1]);
							if (gis.contains(gi)){
								proteins.add(new Protein(gi));
							}
						}
						else{
							if (gis.contains(gi)){
								proteins.lastElement().appendAaBuffer(line.trim());
							}
						}
					}

					for (Protein protein : proteins){
						protein.complete();
					}
				}
				finally {
					fasta_br.close();
					this.proteins = new ArrayList<Protein>(proteins);
					loaded = true;
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void divideProteins(ArrayList<Integer> taxids,
			ArrayList<HashSet<Integer>> queryGISets,
			ArrayList<HashSet<Integer>> databaseGISets) {

		String filenamePrefix = filename.substring(0, filename.lastIndexOf("."));

		int taxid;
		HashSet<Integer> queryGIs, databaseGIs;
		String file1, file2;


		for (int i=0; i<taxids.size(); i++){
			taxid = taxids.get(i);
			queryGIs = queryGISets.get(i);
			databaseGIs = databaseGISets.get(i);

			file1 = filenamePrefix + "_w" + taxid + ".fasta";
			file2 = filenamePrefix + "_wo" + taxid + ".fasta";

			try {
				BufferedReader fastaBR = new BufferedReader(new FileReader(new File(this.filename)));
				BufferedWriter fastaBW1 = new BufferedWriter(new FileWriter(new File(file1))); 
				BufferedWriter fastaBW2 = new BufferedWriter(new FileWriter(new File(file2))); 

				try {
					String line;
					int gi = 0;

					while ((line = fastaBR.readLine()) != null){
						if (line.startsWith(">")){
							gi = Integer.parseInt(line.split("\\|")[1]);

							if (queryGIs.contains(gi)){
								fastaBW1.write(line);
								fastaBW1.newLine();
							}
							if (databaseGIs.contains(gi)){
								fastaBW2.write(line);
								fastaBW2.newLine();
							}
						}
						else{
							if (queryGIs.contains(gi)){
								fastaBW1.write(line);
								fastaBW1.newLine();
							}
							if (databaseGIs.contains(gi)){
								fastaBW2.write(line);
								fastaBW2.newLine();
							}
						}
					}
				}
				finally {
					fastaBR.close();
					fastaBW1.close();
					fastaBW2.close();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
