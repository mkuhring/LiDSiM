/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Vector;

import de.rki.ng4.lidsim.algorithms.ApproximateBoyerMoore;
import de.rki.ng4.lidsim.algorithms.Kmer;
import de.rki.ng4.lidsim.algorithms.WuManber;
import de.rki.ng4.lidsim.bio.Enzyme;
import de.rki.ng4.lidsim.bio.Peptide;
import de.rki.ng4.lidsim.bio.Protein;
import de.rki.ng4.lidsim.bio.Taxonomy;
import net.java.frej.fuzzy.Fuzzy;

public class Search {

	HashMap<Integer,ArrayList<Peptide>> queries;
	ArrayList<Protein> database;

	int numPeptides, numProteins;

	HashMap<Integer,HashMap<String,ArrayList<Match>>> matches;
	int matchCount;

	public Search(HashMap<Integer,ArrayList<Peptide>> queries, ArrayList<Protein> database){
		this.queries = queries;
		this.database = database;

		numPeptides = 0;
		for (ArrayList<Peptide> list : queries.values()){
			numPeptides = numPeptides + list.size();
		}
		numProteins = database.size();
	}
	
	public void runSearch(int tolerance, Kmer kmer, Taxonomy tax){
		if (tolerance==0){
			exactSearch(); // Wu-Manber
		}
		else{
			approximateSearch(tolerance, kmer); // approximate Boyer-Moore
		}

		matchCount = 0;
		for (HashMap<String,ArrayList<Match>> matchList : matches.values()){
			matchCount = matchCount + matchList.size();
		}

		checkMatches();
		checkUniques(matches);
		
		// remove nonuniques, keep closest match 
		removeNonuniques(matches, tax);
	}	

	// record a mismatch (match with target_gi=0) if a query has no matches
	private void checkMatches(){
		for (Entry<Integer, ArrayList<Peptide>> entry : queries.entrySet()){
			int taxid = entry.getKey();
			for (Peptide query : entry.getValue()){
				if (!matches.get(taxid).containsKey(query.getId())){
					matches.get(taxid).put(query.getId(), new ArrayList<Match>());
					matches.get(taxid).get(query.getId()).add(
							new Match(query.getId(), query.getGi(), entry.getKey(), 0, 0, 0));
				}
			}
		}
	}

	// check if matches are unique or not
	public static void checkUniques(HashMap<Integer,HashMap<String,ArrayList<Match>>> matches){
		for (HashMap<String, ArrayList<Match>> orgmatches : matches.values()){
			for (ArrayList<Match> matchList : orgmatches.values()){
				if (matchList.size() > 1){
					for (Match match : matchList){
						match.setUnique(false);
					}
				}
			}		
		}
	}

	@SuppressWarnings("unused")
	private void simpleSearch(){

		matches = new HashMap<Integer,HashMap<String,ArrayList<Match>>>();
		HashMap<String,ArrayList<Match>> orgmatches;

		String peptideAA;

		for (Entry<Integer, ArrayList<Peptide>> entry : queries.entrySet()){
			orgmatches = new HashMap<String,ArrayList<Match>>();
			for (Peptide query : entry.getValue()){
				peptideAA = query.getAaSequence();
				for (Protein target : database){
					if (target.getAaSequence().contains(peptideAA)){
						if (!orgmatches.containsKey(query.getId())){
							orgmatches.put(query.getId(), new ArrayList<Match>());
						}
						for (int targetTaxid : target.getTaxids()){
							if (targetTaxid != entry.getKey()){
								orgmatches.get(query.getId()).add(
										new Match(query.getId(), query.getGi(), entry.getKey(), query.getAaSequence().length(), target.getGi(), targetTaxid));
							}
						}
					}
				}
			}
			matches.put(entry.getKey(), orgmatches);
		}
	}

	// searches the queries (peptides) in the database (proteins) with the Appromximate Boyer-Moore to allow errors
	private void approximateSearch(int tolerance, Kmer kmer){
		matches = new HashMap<Integer,HashMap<String,ArrayList<Match>>>();
		HashMap<String,ArrayList<Match>> orgmatches;

		HashSet<Integer> kmerMatches = null;
		ApproximateBoyerMoore abmSearch;

		for (Entry<Integer, ArrayList<Peptide>> entry : queries.entrySet()){
			orgmatches = new HashMap<String,ArrayList<Match>>();
			for (Peptide query : entry.getValue()){

				if (kmer != null){
					kmerMatches = kmer.search(query);
				}
				abmSearch = new ApproximateBoyerMoore(query.getAaSequence(), tolerance);

				//			System.out.println(" searching in " + kmerMatches.size() + " of " + database.size());

				for (Protein target : database){
					if (kmerMatches == null || kmerMatches.contains(target.getGi())){
						if (abmSearch.isIn(target.getAaSequence())){
							if (!orgmatches.containsKey(query.getId())){
								orgmatches.put(query.getId(), new ArrayList<Match>());
							}
							for (int targetTaxid : target.getTaxids()){
								if (targetTaxid != entry.getKey()){
									orgmatches.get(query.getId()).add(
											new Match(query.getId(), query.getGi(), entry.getKey(), query.getAaSequence().length(), target.getGi(), targetTaxid));
								}
							}
						}
					}
				}
			}
			matches.put(entry.getKey(), orgmatches);
		}
	}

	// searches the queries (peptides) in the database (proteins) with the Fuzzy String Matching to allow errors
	@SuppressWarnings("unused")
	private void tolerantSearch(int tolerance){
		matches = new HashMap<Integer,HashMap<String,ArrayList<Match>>>();
		HashMap<String,ArrayList<Match>> orgmatches;

		double normalizedBestDistance;
		double unnormalizedBestDistance;
		double minDistance;

		ArrayList<Peptide> target_digestions;
		Fuzzy fuzzy = new Fuzzy();

		for (Protein target : database){

			if (target.getAaSequence().length() > 256){
				target_digestions = Enzyme.digest(target);

				for (Entry<Integer, ArrayList<Peptide>> entry : queries.entrySet()){
					orgmatches = new HashMap<String,ArrayList<Match>>();
					for (Peptide query : entry.getValue()){
						// get best substring Demerau-Levenshtein distance between target and query
						minDistance = Double.MAX_VALUE;
						for (Peptide sub_target : target_digestions){
							// Fuzzy ist statisch, parallel aufruf nicht m�glich???
							normalizedBestDistance = fuzzy.containability(sub_target.getAaSequence(), query.getAaSequence());
							unnormalizedBestDistance = normalizedBestDistance * query.getAaSequence().length();
							minDistance = Math.min(minDistance, unnormalizedBestDistance);
						}

						// compare distance to tolerance
						if (minDistance <= tolerance){
							if (!orgmatches.containsKey(query.getId())){
								orgmatches.put(query.getId(), new ArrayList<Match>());
							}
							for (int targetTaxid : target.getTaxids()){
								if (targetTaxid != entry.getKey()){
									orgmatches.get(query.getId()).add(new Match(query.getId(), query.getGi(), entry.getKey(), query.getAaSequence().length(), target.getGi(), targetTaxid));
								}
							}
						}
					}
					matches.put(entry.getKey(), orgmatches);
				}
			}

			else{
				for (Entry<Integer, ArrayList<Peptide>> entry : queries.entrySet()){
					orgmatches = new HashMap<String,ArrayList<Match>>();
					for (Peptide query : entry.getValue()){
						// get best substring Demerau-Levenshtein distance between target and query
						normalizedBestDistance = fuzzy.containability(target.getAaSequence(), query.getAaSequence());
						unnormalizedBestDistance = normalizedBestDistance * query.getAaSequence().length();

						// compare distance to tolerance
						if (unnormalizedBestDistance <= tolerance){
							if (!orgmatches.containsKey(query.getId())){
								orgmatches.put(query.getId(), new ArrayList<Match>());
							}
							for (int targetTaxid : target.getTaxids()){
								if (targetTaxid != entry.getKey()){
									orgmatches.get(query.getId()).add(new Match(query.getId(), query.getGi(), entry.getKey(), query.getAaSequence().length(), target.getGi(), targetTaxid));
								}
							}
						}
					}
					matches.put(entry.getKey(), orgmatches);
				}
			}
		}
	}

	// searches the queries (peptides) in the database (proteins) with the Wu-Manber algorithm (exact multi pattern matching)
	private void exactSearch(){
		matches = new HashMap<Integer,HashMap<String,ArrayList<Match>>>();

		// extract all peptide sequences
		ArrayList<Integer> taxids = new ArrayList<Integer>();
		ArrayList<Peptide> peptides = new ArrayList<Peptide>();
		Vector<String> patterns = new Vector<String>();

		for (Entry<Integer, ArrayList<Peptide>> entry : queries.entrySet()){

			matches.put(entry.getKey(), new HashMap<String,ArrayList<Match>>());

			for (Peptide peptide : entry.getValue()){
				taxids.add(entry.getKey());
				peptides.add(peptide);
				patterns.add(peptide.getAaSequence());
			}
		}

		// prepare string matching
		// optimaler Wert f�r AlphabetSize? Anzahl m�glicher Aminos�uren?
		WuManber matcher = new WuManber(patterns, 21);

		// iterate over all proteins in database
		HashMap<Integer, Vector<Integer>> results;
		Match match;
		for (Protein target : database){
			results = matcher.searchIn(target.getAaSequence());

			if (!results.isEmpty()){
				for (int queryIndex : results.keySet()){
					int queryTaxid = taxids.get(queryIndex);
					if (!matches.get(queryTaxid).containsKey(peptides.get(queryIndex).getId())){
						matches.get(queryTaxid).put(peptides.get(queryIndex).getId(), new ArrayList<Match>());
					}
					for (int targetTaxid : target.getTaxids()){
						if (targetTaxid != taxids.get(queryIndex)){
							match = new Match(peptides.get(queryIndex).getId(), peptides.get(queryIndex).getGi(), taxids.get(queryIndex), peptides.get(queryIndex).getAaSequence().length(), target.getGi(), targetTaxid);
							matches.get(queryTaxid).get(peptides.get(queryIndex).getId()).add(match);
						}
					}
				}
			}	
		}
	}

	// returns the matches, including unmatched peptides (as match with target_gi=0) 
	public ArrayList<Match> getMatches(){
		ArrayList<Match> allMatches = new ArrayList<Match>();
		for (HashMap<String, ArrayList<Match>> orgmatches : matches.values()){
			for (ArrayList<Match> matchList : orgmatches.values()){
				allMatches.addAll(matchList);
			}
		}
		return allMatches;
	} // getMatches

	public static void removeNonuniques(HashMap<Integer, HashMap<String, ArrayList<Match>>> matches, Taxonomy tax){

		int lca;
		int queryDist;
		int targetDist;

		int lastQueryDist;
		int lastTargetDist;

		Match match;
		Match bestMatch;

		ArrayList<Match> matchList;

		for (HashMap<String, ArrayList<Match>> orgmatches : matches.values()){
			for (Entry<String, ArrayList<Match>>  matchesEntry : orgmatches.entrySet()){

				matchList = matchesEntry.getValue();
				if (matchList.size() > 1){
					match = matchList.get(0);
					lca = tax.getLowestCommonAncestor(match.getQueryTaxid(), match.getTargetTaxid());
					queryDist = tax.getAncestorDistance(match.getQueryTaxid(), lca);
					targetDist = tax.getAncestorDistance(match.getTargetTaxid(), lca);

					lastQueryDist = queryDist;
					lastTargetDist = targetDist;
					bestMatch = match;

					for (int i=1; i<matchList.size(); i++){
						match = matchList.get(i);
						lca = tax.getLowestCommonAncestor(match.getQueryTaxid(), match.getTargetTaxid());
						queryDist = tax.getAncestorDistance(match.getQueryTaxid(), lca);
						targetDist = tax.getAncestorDistance(match.getTargetTaxid(), lca);

						if (queryDist < lastQueryDist){						
							lastQueryDist = queryDist;
							lastTargetDist = targetDist;
							bestMatch = match;
						}
						else if (queryDist == lastQueryDist && targetDist < lastTargetDist){
							lastQueryDist = queryDist;
							lastTargetDist = targetDist;
							bestMatch = match;
						}
					}

					matchList = new ArrayList<Match>();
					matchList.add(bestMatch);
					matchesEntry.setValue(matchList);
				}
			}
		}
	} // removeNonuniques

	// returns the number of real matched peptides, i.e. without the unmatched peptides
	public int getMatchCount(){
		return matchCount;
	}

	public int getQuerySize(){
		return numPeptides;
	}

	public int getDatabaseSize(){
		return numProteins;
	}
}
