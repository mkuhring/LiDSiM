/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim;

import java.util.HashSet;
import java.util.concurrent.Callable;

import de.rki.ng4.lidsim.algorithms.Kmer;
import de.rki.ng4.lidsim.bio.Taxonomy;

public class SearchTask implements Callable<Boolean> {

	private int setId, setTaxid;
	private HashSet<Integer> queryTaxids;
	private HashSet<Integer> databaseTaxids;
	private Extraction extractor;
	private Taxonomy tax;
	private Result results;
	private String simlevel;
	private int tolerance;
	private Kmer kmer;
	private int pepSampleSize;
	
	public SearchTask(int setId, int setTaxid, HashSet<Integer> giSet, HashSet<Integer> dbSet, Extraction extractor, Taxonomy tax, Result results, String simlevel, int tolerance, Kmer kmer, int pepSampleSize){
		
		this.setId = setId;
		this.setTaxid = setTaxid;
		this.queryTaxids = giSet;
		this.databaseTaxids = dbSet;
		this.extractor = extractor;
		this.tax = tax;
		this.results = results;
		this.simlevel = simlevel;
		this.tolerance = tolerance;
		this.kmer = kmer;
		this.pepSampleSize = pepSampleSize;
	}
	
	@Override
	public Boolean call() throws Exception {
		Timer searchtimer = new Timer();
		searchtimer.start();
		System.out.println("set " + setId  + " [" + setTaxid + "]: taxids " + queryTaxids.toString());	
		
		// search targets in database with k mismatches
		Search search = new Search(extractor.getQueries(queryTaxids, pepSampleSize), extractor.getDatabase(databaseTaxids));
		System.out.println("set " + setId  + " [" + setTaxid + "]: searching " + search.getQuerySize() + " peptides in " + search.getDatabaseSize() + " proteins...");		
		search.runSearch(tolerance, kmer, tax);
		
		// calc taxonomic relations and export search results to file
		results.export(search.getMatches(), setId, setTaxid, simlevel);
		
		System.out.print("set " + setId + " [" + setTaxid + "]: matched " + search.getMatchCount() + " of " + search.getQuerySize() + " peptides... ");
		searchtimer.stop();
		
		return null;
	}
}
