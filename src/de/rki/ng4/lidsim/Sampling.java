/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import de.rki.ng4.lidsim.bio.Taxonomy;

public class Sampling {

	private Taxonomy tax;

	private ArrayList<HashSet<Integer>> querySets;
	private ArrayList<HashSet<Integer>> databaseSets;

	private Random generator;

	public Sampling(Taxonomy tax, long seed){
		this.tax = tax;
		generator = new Random(seed);
	}

	public Sampling(Taxonomy tax) {
		this.tax = tax;
		generator = new Random();
	}

	public ArrayList<HashSet<Integer>> getQueries() {
		return querySets;
	}

	public ArrayList<HashSet<Integer>> getDatabases() {
		return databaseSets;
	}

	/* for each taxid in taxids, extractByTaxids calculates a query and 
	 * database set such that the query contains the subtree with root taxid
	 */
	public void extractByTaxid(ArrayList<Integer> taxids){

		querySets = new ArrayList<HashSet<Integer>>();
		databaseSets = new ArrayList<HashSet<Integer>>();

		HashSet<Integer> querySet, databaseSet;

		for (int node : taxids){
			querySet = new HashSet<Integer>();
			databaseSet = new HashSet<Integer>();

			querySet = tax.getDescendants(node);

			for (int taxid : tax.getAllTaxids()){
				if (!querySet.contains(taxid)){
					databaseSet.add(taxid);
				}
			}

			querySets.add(querySet);
			databaseSets.add(databaseSet);
		}
	}
	
	/* For each pair of extractionTaxid and sampleTaxid, extractByTaxid makes
	 * query with sampleSize samples from sampleTaxid subtree leafs, if sampleTaxid
	 * is a descendant from extractionTaxid. 
	 * The database will exclude the extractionTaxid subtree.
	 */
	public ArrayList<Integer> extractByTaxid(ArrayList<Integer> extractionTaxids, ArrayList<Integer> sampleTaxids, int sampleSize){
		querySets = new ArrayList<HashSet<Integer>>();
		databaseSets = new ArrayList<HashSet<Integer>>();

		// check if the sampleTaxids are descendants of corresponding extractionTaxids
		ArrayList<String> errors = new ArrayList<String>();
		for (int i=0; i<extractionTaxids.size(); i++){
			int extractionTaxid = extractionTaxids.get(i);
			int sampleTaxid = sampleTaxids.get(i);
			if (!tax.getDescendants(extractionTaxid).contains(sampleTaxid)){
				errors.add("error: " + sampleTaxid + " is no descendant of " + extractionTaxid);
			}	
		}
		if (errors.size()>0){
			for (String error : errors){
				System.out.println(error);
			}
			System.exit(1);
		}
		
		for (int i=0; i<extractionTaxids.size(); i++){
			querySets.add(nodeSampling(sampleTaxids.get(i), sampleSize));
			databaseSetConstruction(extractionTaxids.get(i));
		}
		
		return extractionTaxids;
	}
	
	/* Uses just one sampleTaxid for all extractionTaxids, assuming that it is a
	 * descendant of all extractionTaxids. Thus, every extraction will get the
	 * same sample set. Useful for different level extraction,
	 * e.g. sample ecoli and extract species, genus, family...
	 * The database will exclude the extractionTaxid subtree.
	 */
	public ArrayList<Integer> extractByTaxid(ArrayList<Integer> extractionTaxids, int sampleTaxid, int sampleSize){
		querySets = new ArrayList<HashSet<Integer>>();
		databaseSets = new ArrayList<HashSet<Integer>>();

		// check if the sampleTaxids are descendants of corresponding extractionTaxids
		ArrayList<String> errors = new ArrayList<String>();
		for (int i=0; i<extractionTaxids.size(); i++){
			int extractionTaxid = extractionTaxids.get(i);
			if (!tax.getDescendants(extractionTaxid).contains(sampleTaxid)){
				errors.add("error: " + sampleTaxid + " is no descendant of " + extractionTaxid);
			}	
		}
		if (errors.size()>0){
			for (String error : errors){
				System.out.println(error);
			}
			System.exit(1);
		}
		
		HashSet<Integer> samples = nodeSampling(sampleTaxid, sampleSize);
		for (int i=0; i<extractionTaxids.size(); i++){
			querySets.add(samples);
			databaseSetConstruction(extractionTaxids.get(i));
		}
		
		return extractionTaxids;
	}

	public ArrayList<Integer> calcSets(String extractionRank, String sampleRank, int sampleSize){

		querySets = new ArrayList<HashSet<Integer>>();
		databaseSets = new ArrayList<HashSet<Integer>>();

		// get extractionRank nodes
		ArrayList<Integer> extractionRankNodes =  new ArrayList<Integer>(tax.taxidsWithRank(extractionRank));

		for (int node : extractionRankNodes){
			querySetSampling(node, sampleRank, sampleSize);
			databaseSetConstruction(node);
		}
		
		return extractionRankNodes;
	}

	// for a give node, sample the leafs
	private HashSet<Integer> nodeSampling(int sampleNode, int sampleSize){

		HashSet<Integer> leafs;
		ArrayList<Integer> leafList;
		int random;

		HashSet<Integer> samples = new HashSet<Integer>();

		// iterate over each sampleNodesOfExtractionNode to sample the given number of organism
		leafs = tax.getDescendantLeafs(sampleNode);				

		if (leafs.size() <= sampleSize || sampleSize <= 0){
			samples.addAll(leafs);
		}
		else{
			leafList = new ArrayList<Integer>(leafs);
			while (samples.size() < sampleSize){
				random = generator.nextInt(leafList.size());
				samples.add(leafList.remove(random));
			}
		}

		return samples;
	}
	
	private void querySetSampling(int extractionNode, String sampleRank, int sampleSize){

//		HashSet<Integer> leafs, temp;
//		ArrayList<Integer> leafList;
//		int random;

		HashSet<Integer> querySet = new HashSet<Integer>();

		HashSet<Integer> sampleNodes = tax.getDescendantsWithRank(extractionNode, sampleRank);
	
		// iterate over each sampleNodesOfExtractionNode to sample the given number of organism
		for (int sampleNode : sampleNodes){

//			leafs = tax.getDescendantLeafs(sampleNode);				
//
//			temp = new HashSet<Integer>();
//
//			if (leafs.size() <= sampleSize || sampleSize <= 0){
//				temp.addAll(leafs);
//			}
//			else{
//				leafList = new ArrayList<Integer>(leafs);
//				while (temp.size() < sampleSize){
//					random = generator.nextInt(leafList.size());
//					temp.add(leafList.remove(random));
//				}
//			}
//			querySet.addAll(temp);
			
			querySet.addAll(nodeSampling(sampleNode, sampleSize));
		}
		
		querySets.add(querySet);
	}
	
	private void databaseSetConstruction(int extractionNode){
		HashSet<Integer> descendants = tax.getDescendants(extractionNode);
		HashSet<Integer> databaseSet = new HashSet<Integer>();
		// iterate over taxids, if not in extractionRank nodes -> put in database
		for (int taxid : tax.getAllTaxids()){
			if (!descendants.contains(taxid)){
				databaseSet.add(taxid);
			}
		}
		databaseSets.add(databaseSet);
	}


	//	// return sets of GIs pooled by organism (i.e. same taxid)
	//	public ArrayList<HashSet<Integer>> getOrganismSets(HashMap<Integer, HashSet<Integer>> taxidToGi){
	//
	//		ArrayList<HashSet<Integer>> giSets = new ArrayList<HashSet<Integer>>();
	//
	//		giSets.addAll(taxidToGi.values());
	//
	//		return giSets;
	//	}
	//	
	//	public void getSetsPerRank(Taxonomy tax, String rank, int samplesize){
	//
	//		ArrayList<HashSet<Integer>> giSets = new ArrayList<HashSet<Integer>>();
	//
	//		if (rank.matches("organism")){
	//			giSets = getOrganismSets(tax.getTaxidToGi());
	//		}
	//		else{
	//			System.out.println(tax.getRanks().toString());
	//
	//			HashSet<Integer> nodes = tax.taxidsWithRank(rank);
	//
	//			HashSet<Integer> orgs;
	//			ArrayList<Integer> orgsList;
	//			HashSet<Integer> temp;
	//			int random;
	//
	//			for (int node : nodes){
	//				orgs = tax.orgsWithAncestor(node);
	//				temp = new HashSet<Integer>();
	//				if (orgs.size() == 0){
	//					temp.addAll(tax.getTaxidToGi().get(node));
	//				}
	//				else if (samplesize <= 0){
	//					temp.addAll(tax.getTaxidToGi().get(node));
	//					for (int org : orgs){
	//						temp.addAll(tax.getTaxidToGi().get(org));
	//					}
	//				}
	//				else{
	//					orgsList = new ArrayList<Integer>(orgs);
	//					orgsList.add(node);
	//					while (temp.size() < Math.min(samplesize, orgsList.size())){
	//						random = generator.nextInt(orgs.size());
	//						temp.addAll(tax.getTaxidToGi().get(orgsList.get(random)));
	//					}
	//				}
	//				giSets.add(temp);
	//			}
	//		}
	//
	//		querySets = giSets;
	//	}
	//
	//	// return sets of GIs pooled by rank (i.e. per species)
	//	public ArrayList<HashSet<Integer>> getRankSets(HashMap<String, HashSet<Integer>> rankToTaxid,
	//			HashMap<Integer,HashSet<Integer>> nodeToLeafs,
	//			HashMap<Integer, HashSet<Integer>> taxidToGi, String rank){
	//
	//		HashSet<Integer> taxids = rankToTaxid.get(rank);
	//
	//		ArrayList<HashSet<Integer>> giSets = new ArrayList<HashSet<Integer>>();
	//
	//		HashSet<Integer> leafs;
	//		HashSet<Integer> gis;
	//
	//		for (Integer taxid : taxids){
	//			leafs = nodeToLeafs.get(taxid);
	//			gis = new HashSet<Integer>();
	//			for (Integer leaf : leafs){
	//				gis.addAll(taxidToGi.get(leaf));
	//			}
	//			giSets.add(gis);
	//		}
	//
	//		giSets.addAll(taxidToGi.values());
	//
	//		return giSets;
	//	}
}
