/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import de.rki.ng4.lidsim.algorithms.Kmer;
import de.rki.ng4.lidsim.bio.Protein;
import de.rki.ng4.lidsim.bio.Taxonomy;
import de.rki.ng4.lidsim.io.Fasta;

public class Main {

	public static void main(String[] args) throws Exception {

		Timer totalTime = new Timer();
		totalTime.start();

		// parameter parsing
		int index = 0;
		
		String fasta_file = args[index++];
		String genome_file = args[index++];		
		String gi_taxid_file = args[index++];
		String tax_node_file = args[index++];
		String result_file = args[index++];

		int tolerance = Integer.parseInt(args[index++]);

		String extractionMode = args[index++];
		
		String extractionRank = args[index++];
		String sampleRank = args[index++];
		
		int taxSampleSize = Integer.parseInt(args[index++]);
		int pepSampleSize = Integer.parseInt(args[index++]);
		long seed = Long.parseLong(args[index++]);
		
		int threads = Integer.parseInt(args[index++]);
		String proteinMode = args[index++];
		
		// just for internal use and debugging
//		int start = Integer.parseInt(args[index++]);
//		start = Math.max(0, start-1);
		int start = 0;
		
		// kmer filter is to slow, thus not used anymore!
//		boolean filter = Boolean.parseBoolean(args[index++]);
		boolean filter = false;
		

		System.out.println();	
		System.out.println("parameter");
		System.out.println(" input files:");
		System.out.println("  fasta:     " + fasta_file);
		System.out.println("  genome:    " + genome_file);
		System.out.println("  gi_taxid:  " + gi_taxid_file);
		System.out.println("  tax_nodes: " + tax_node_file);
		System.out.println(" output files:");
		System.out.println("  results: " + result_file);
		System.out.println(" tolerance: " + tolerance);
		System.out.println(" extraction mode: " + extractionMode);
		System.out.println(" extraction rank (or taxids): " + extractionRank);
		System.out.println(" sample rank (or taxids): " + sampleRank);
		System.out.println(" samples per rank: " + taxSampleSize);
		System.out.println(" samples per org: " + pepSampleSize);
		System.out.println(" sample seed: " + seed);
		System.out.println(" threads: " + threads);
		System.out.println(" protein mode: " + proteinMode);
		System.out.println();		

		Timer timer = new Timer();

		timer.start();
		System.out.print("fasta indexing... ");
		Fasta fasta = new Fasta(fasta_file);
		timer.stop(); 

		timer.start();
		System.out.print("gi extraction... ");
		HashSet<Integer> gis = fasta.getGIs();
		timer.stop(); 

		timer.start();
		System.out.print("gi-taxid import... ");
		Taxonomy tax = new Taxonomy();
		tax.parseGiTaxidFile(gi_taxid_file, gis);
		
		int totalTax = tax.countTaxids();
		System.out.print(totalTax + " orgs in total... ");
		
		ArrayList<Integer> removed = tax.removeGisWithNoTaxidMapping(gis);
		if (removed.size() > 0) System.out.print("removed " + removed.size() + " missing gis... ");
		timer.stop(); 

		timer.start();
		System.out.print("tax nodes import... ");
		tax.parseNodes(tax_node_file);
		timer.stop(); 

		timer.start();
		System.out.print("lineage calculations... ");
		tax.calcLineage();
		tax.introduceSpeciesSubLevel();
		tax.reassignProteins();
		timer.stop(); 

		timer.start();
		System.out.print("sample orgs... ");
		Sampling sampler = new Sampling(tax, seed);
		ArrayList<Integer> setTaxids = new ArrayList<Integer>();
		if (extractionMode.matches("complete")){
			setTaxids = sampler.calcSets(extractionRank, sampleRank, taxSampleSize);
		}
		boolean reset = false;
		if (extractionMode.matches("specific")){
			ArrayList<Integer> extractionTaxids = new ArrayList<Integer>();
			ArrayList<Integer> sampleTaxids = new ArrayList<Integer>();
			
			String[] splits = extractionRank.split(",");
			for (String split : splits){
				extractionTaxids.add(Integer.parseInt(split));
			}
			splits = sampleRank.split(",");
			for (String split : splits){
				sampleTaxids.add(Integer.parseInt(split));
			}
			
			if (sampleTaxids.size() == 1){
				setTaxids = sampler.extractByTaxid(extractionTaxids, sampleTaxids.get(0), taxSampleSize);
				reset = true;
			}
			else{
				setTaxids = sampler.extractByTaxid(extractionTaxids, sampleTaxids, taxSampleSize);				
			}
		}
		ArrayList<HashSet<Integer>> queryTaxidSets = sampler.getQueries();
		ArrayList<HashSet<Integer>> databaseTaxidSets = sampler.getDatabases();
		System.out.print(queryTaxidSets.size() + " sets... ");
				
		timer.stop(); 
	
		timer.start();
		System.out.print("load proteins... ");
		fasta.loadProteins(proteinMode);
		timer.stop(); 

		timer.start();
		System.out.print("prepare extraction (I->L, digestion, length filter)... ");
		Extraction extractor = new Extraction(fasta, tax, seed, reset);
		timer.stop(); 

		timer.start();
		if (!genome_file.matches("none")){
			System.out.print("load genome... ");
			// add dummy gi taxid mapping for gi -1 to taxid 1
			// -1 denotes genome translations without annotation
			// map to the root for max distance in tree
			tax.addGenomeDummyGI();
			Fasta genomeTranslations = new Fasta(genome_file);
			removed = tax.removeGisWithNoTaxidMapping(genomeTranslations.getGIs());
			if (removed.size() > 0) System.out.print("removed " + removed.size() + " missing gis... ");
			extractor.appendProteins(genomeTranslations);
		}
		timer.stop();
		
		// kmer filter is to slow, thus not used!
		// kmer filter won't work with genome file!
		Kmer kmer = null;
		if (filter){
			timer.start();
			System.out.print("kmer index... ");
			kmer = new Kmer(new ArrayList<Protein>(fasta.getProteins()), 4, tax);
			timer.stop();
		}

		timer.start();
		System.out.println("extract and search (" + queryTaxidSets.size() + " sets)... ");
		Search search;
		Result results = new Result(result_file, tax);
		String simlevel = extractionRank;

		// iterate over all sets
		if (threads == 1){
			Timer searchtimer;
			for (int i=start; i<queryTaxidSets.size(); i++){
				searchtimer = new Timer();
				searchtimer.start();
				System.out.println("set " + (i+1)  + " [" + setTaxids.get(i) + "]: taxids " + queryTaxidSets.get(i).toString());	

				// search targets in database with k mismatches
				search = new Search(extractor.getQueries(queryTaxidSets.get(i), pepSampleSize), extractor.getDatabase(databaseTaxidSets.get(i)));
				System.out.println("set " + (i+1) + " [" + setTaxids.get(i) + "]: searching " + search.getQuerySize() + " peptides in " + search.getDatabaseSize() + " proteins...");
				search.runSearch(tolerance, kmer, tax);

				// calc taxonomic relations and export search results to file
				results.export(search.getMatches(), i+1, setTaxids.get(i), simlevel);

				System.out.print("set " + (i+1) + " [" + setTaxids.get(i) + "]: matched " + search.getMatchCount() + " of " + search.getQuerySize()  + " peptides... ");
				searchtimer.stop();
			}
		}
		// multi-threaded iteration
		else{
			final ExecutorService producers = Executors.newFixedThreadPool(threads);
			for (int i=start; i<queryTaxidSets.size(); i++){
				producers.submit(new SearchTask(i+1, setTaxids.get(i), queryTaxidSets.get(i), databaseTaxidSets.get(i), 
						extractor, tax, results, simlevel, tolerance, kmer, pepSampleSize));
			}
			producers.shutdown();
			producers.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		}
		results.close();
		timer.stop(); 

		System.out.print("done, total time: ");
		totalTime.stop();
	}
}


