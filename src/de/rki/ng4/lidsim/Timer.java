/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim;

public class Timer {
	// simple timer, use with timerStart() and timerStop()
	public long timer = 0;

	// starts a simple timer
	public void start(){
		timer = System.currentTimeMillis();
	}

	// stops the simple timer
	public void stop(){
		if (timer == 0)  System.out.println("warning: timer not startet");
		else System.out.println((System.currentTimeMillis() -  timer)/1000 + " sec");	
	}
}
