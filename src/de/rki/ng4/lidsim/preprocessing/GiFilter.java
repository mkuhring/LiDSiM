/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.preprocessing;

import java.util.HashSet;

import de.rki.ng4.lidsim.Timer;
import de.rki.ng4.lidsim.bio.Taxonomy;
import de.rki.ng4.lidsim.io.Fasta;

public class GiFilter {

	public static void main(String[] args) throws Exception {
		if (args.length != 3){
			System.out.println("gifilter extracts the gi-taxid-mappings for the gis available in a fasta file");
			System.out.println("usage: filter FASTA.FILE GI.TAXID.FILE OUTPUT.FILE");
			System.exit(0);
		}
		
		String fasta_filename = args[0];
		String taxid_filename = args[1];
		String output_filename = args[2];
			
		// read fasta to extract the gis
		System.out.print("reading fasta... ");
		Timer timer = new Timer();
		timer.start();
		Fasta fasta = new Fasta(fasta_filename);
		HashSet<Integer> gis = fasta.getGIs();
		timer.stop();
			
		// use gis to filter Gi-Taxid File
		System.out.print("reading taxids... ");
		timer.start();
		Taxonomy.filterGiTaxidFile(taxid_filename, output_filename, gis);		
		timer.stop();
	}

}
