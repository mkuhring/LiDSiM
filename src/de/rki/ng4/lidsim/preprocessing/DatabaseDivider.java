/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim.preprocessing;

import java.util.ArrayList;
import java.util.HashSet;

import de.rki.ng4.lidsim.Sampling;
import de.rki.ng4.lidsim.Timer;
import de.rki.ng4.lidsim.bio.Taxonomy;
import de.rki.ng4.lidsim.io.Fasta;

public class DatabaseDivider {

	public static void main(String[] args) {
		
		Timer totalTime = new Timer();
		totalTime.start();
		
		int index = 0;
		
		// files
		String fasta_file = args[index++];
		String gi_taxid_file = args[index++];
		String tax_node_file = args[index++];
		
		// list of taxids for subtree extraction
		String tmpTaxids = args[index++];
		ArrayList<Integer> taxids = new ArrayList<Integer>();
		for (String split : tmpTaxids.split(",")){
			taxids.add(Integer.parseInt(split));
		}

		System.out.println();	
		System.out.println("parameter");
		System.out.println(" input files:");
		System.out.println("  fasta:     " + fasta_file);
		System.out.println("  gi_taxid:  " + gi_taxid_file);
		System.out.println("  tax_nodes: " + tax_node_file);
		System.out.println(" taxids: " + taxids.toString());
		System.out.println();		

		Timer timer = new Timer();

		timer.start();
		System.out.print("fasta indexing... ");
		Fasta fasta = new Fasta(fasta_file);
		timer.stop(); 

		timer.start();
		System.out.print("gi extraction... ");
		HashSet<Integer> gis = fasta.getGIs();
		timer.stop(); 

		timer.start();
		System.out.print("gi-taxid import... ");
		Taxonomy tax = new Taxonomy();
		tax.parseGiTaxidFile(gi_taxid_file, gis);
		
		int totalTax = tax.countTaxids();
		System.out.print(totalTax + " orgs in total... ");
		
		ArrayList<Integer> removed = tax.removeGisWithNoTaxidMapping(gis);
		if (removed.size() > 0) System.out.print("removed " + removed.size() + " missing gis... ");
		timer.stop(); 

		timer.start();
		System.out.print("tax nodes import... ");
		tax.parseNodes(tax_node_file);
		timer.stop(); 

		timer.start();
		System.out.print("lineage calculations... ");
		tax.calcLineage();
		timer.stop(); 

		timer.start();
		System.out.print("sample orgs... ");
		Sampling sampler = new Sampling(tax);
		sampler.extractByTaxid(taxids);
		ArrayList<HashSet<Integer>> queryTaxidSets = sampler.getQueries();
		ArrayList<HashSet<Integer>> databaseTaxidSets = sampler.getDatabases();
		System.out.print(queryTaxidSets.size() + " sets... ");
		timer.stop(); 
		System.out.println(queryTaxidSets.get(0).toString());
		
		// get gis for each taxidset
		ArrayList<HashSet<Integer>> queryGISets = new ArrayList<HashSet<Integer>>();
		ArrayList<HashSet<Integer>> databaseGISets = new ArrayList<HashSet<Integer>>();
		for (int i=0; i<taxids.size(); i++){
			queryGISets.add(tax.getGis(queryTaxidSets.get(i)));
			databaseGISets.add(tax.getGis(databaseTaxidSets.get(i)));
		}
		
		timer.start();
		System.out.print("load and divide proteins... ");
		fasta.divideProteins(taxids, queryGISets, databaseGISets);
		timer.stop(); 

		System.out.print("done... ");
		totalTime.stop();
	}

}
