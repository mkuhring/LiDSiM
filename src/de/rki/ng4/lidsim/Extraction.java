/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import de.rki.ng4.lidsim.bio.Enzyme;
import de.rki.ng4.lidsim.bio.Peptide;
import de.rki.ng4.lidsim.bio.Protein;
import de.rki.ng4.lidsim.bio.Taxonomy;
import de.rki.ng4.lidsim.io.Fasta;

public class Extraction {

	private Taxonomy tax;

	private Random generator;
	private long seed;
	private boolean reset;

	private ArrayList<Protein> proteins;
	private ArrayList<Protein> dbReplacement;
	private ArrayList<Protein> dbAddition;
	private boolean replacedProteins = false;
	private boolean appendedProteins = false;
//	private ArrayList<ArrayList<Peptide>> peptides;

	public Extraction(Fasta fasta, Taxonomy tax, long seed, boolean reset){
		this.tax = tax;

		this.seed = seed;
		this.reset = reset;
		if (seed == 0){
			generator = new Random(); 
			this.reset=false;
		}
		else{
			generator = new Random(seed);
		}

		proteins = new ArrayList<Protein>(fasta.getProteins());
		assignTaxids(proteins);
		
		// Change I to L before or after digestion? 
		// It's faster before, since only the proteins have to be processed. Afterwards, the proteins and the new peptides have to be processed.
		replaceAas();
	}

	// return the queries (digested peptides), version for threads
	public HashMap<Integer,ArrayList<Peptide>> getQueries(HashSet<Integer> taxids, int maxPeptides){
		HashMap<Integer,ArrayList<Peptide>> queries = new HashMap<Integer,ArrayList<Peptide>>();

		HashSet<Integer> gis;
		ArrayList<Peptide> nodePeptides;

		for (int taxid : taxids){
			gis = tax.getGis(taxid);
			nodePeptides = new ArrayList<Peptide>();

			// collect peptides for taxid/node
			for (int i=0; i<proteins.size(); i++){
				if (gis.contains(proteins.get(i).getGi())){
//					nodePeptides.addAll(peptides.get(i));
					nodePeptides.addAll(Enzyme.digest(proteins.get(i)));
				}
			}

			// sample down
			queries.put(taxid, new ArrayList<Peptide>(samplePeptides(nodePeptides, maxPeptides)));
//			queries.addAll(samplePeptides(nodePeptides, maxPeptides));
		}
		return queries;
	}

	// return the database (proteins), version for threads
	public ArrayList<Protein> getDatabase(HashSet<Integer> taxids){
		
		if (replacedProteins) return dbReplacement;
		
		ArrayList<Protein> database = new ArrayList<Protein>();
		
		HashSet<Integer> gis = tax.getGis(taxids);
		for (Protein protein : proteins){
			if (gis.contains(protein.getGi())){
				database.add(protein);
			}
		}
		
		if (appendedProteins) database.addAll(dbAddition);
		
		return database;
	}

	// randomly draw a given number of Peptides from a given set
	private synchronized ArrayList<Peptide> samplePeptides(ArrayList<Peptide> peptides, int maxNumber){
		ArrayList<Peptide> subset = new  ArrayList<Peptide>();

		if (reset) generator.setSeed(seed);
		
		if (peptides.size() <= maxNumber || maxNumber <= 0){
			subset.addAll(peptides);
		}
		else{
			while (subset.size() < maxNumber){
				subset.add(peptides.remove(generator.nextInt(peptides.size())));
			}
		}

		return subset;
	}

	// substitute 'I' to 'L' in every protein.
	// Their mass is the same, so they should match in a search.
	private void replaceAas(){
		for (Protein protein : proteins){
			protein.replaceAA('I', 'L');
		}
	}	
	
	private void assignTaxids(ArrayList<Protein> proteins){
		for(Protein protein : proteins){
			protein.setTaxids(tax.getTaxids(protein.getGi()));
		}
	}
	
	// replace all proteins
	public void replaceProteins(Fasta fasta){
		dbReplacement = new ArrayList<Protein>(fasta.getProteins());
		assignTaxids(dbReplacement);
		replacedProteins = true;
	}
	
	// append other proteins
	public void appendProteins(Fasta fasta){
		dbAddition = new ArrayList<Protein>(fasta.getProteins());
		assignTaxids(dbAddition);
		appendedProteins = true;
	}
}
