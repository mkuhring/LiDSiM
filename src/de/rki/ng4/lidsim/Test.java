/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim;

import java.util.ArrayList;
import java.util.HashMap;



//import ru.fuzzysearch.BitapOnlineSearcher;
//import ru.fuzzysearch.EnglishAlphabet;
import net.java.frej.fuzzy.Fuzzy;
import de.rki.ng4.lidsim.bio.Enzyme;
import de.rki.ng4.lidsim.bio.Peptide;
import de.rki.ng4.lidsim.bio.Protein;

public class Test {

	public static void main(String[] args){

		fuzzyTest();

	}
	
	@SuppressWarnings("unused")
	private static void HashMapTest(){
		HashMap<Integer, ArrayList<Match>> test = new HashMap<Integer, ArrayList<Match>>();
		int check = 123;
		boolean content = test.containsKey(check);
		System.out.println(content);
		test.put(check, new ArrayList<Match>());
		content = test.containsKey(check);
		System.out.println(content);
	}
	
//	private static void bitapTest(){
//		BitapOnlineSearcher bitap = new BitapOnlineSearcher(new EnglishAlphabet());
//		Reader text = new StringReader("MLMKPYISHRAIICTLTLLIIIITVLFTFLRSSDVPEYITAPVRKPGDIENSVLATGRIDAIERVNVGAQVSGQLKSLKVKQ");
//		ArrayList<String> pattern = new ArrayList<String>();
//		// 0 error
//		pattern.add("IITVLFTFLRSSD");
//		pattern.add("VRKPGDIENSVLA");
//		pattern.add("YISHRAI");
//		// 1 error
//		pattern.add("IITVLFFTLRSSD");
//		pattern.add("VRVPGDIENSVLA");
//		pattern.add("YISIRAI");
//		// 2 error
//		pattern.add("IIVTLFTFLRDSD");
//		pattern.add("VRAPGDIESVLA");
//		pattern.add("YIAHRII");
//		
//		for (String p : pattern){
//			System.out.println(bitap.search(text, p, 2));
//		}
//	}
	
	private static void fuzzyTest(){
		
		String text = "MLMKPYISHRAIICTLTLLIIIITVLFTFLRSSDVPEYITAPVRKPGDIENSVLATGRIDAIERVNVGAQVSGQLKSLKVKQ";
		ArrayList<String> pattern = new ArrayList<String>();
		// 0 error
		pattern.add("IITVLFTFLRSSD");
		pattern.add("VRKPGDIENSVLA");
		pattern.add("YISHRAI");
		// 1 error
		pattern.add("IITVLFFTLRSSD");
		pattern.add("VRVPGDIENSVLA");
		pattern.add("YISIRAI");
		// 2 error
		pattern.add("IIVTLFTFLRDSD");
		pattern.add("VRAPGDIESVLA");
		pattern.add("YIAHRII");
		
		Fuzzy fuzzy = new Fuzzy();
		for (String p : pattern){
			System.out.println(fuzzy.containability(text, p) * p.length());
		}
	}
	

	@SuppressWarnings("unused")
	private static void digestionTest(){

		ArrayList<Protein> testProts = new ArrayList<Protein>();
		testProts.add(new Protein(123, "MLMKPYISHRAIICTLTLLIIIITVLFTFLRSSDVPEYITAPVRKPGDIENSVLATGRIDAIERVNVGAQVSGQLKSLKVKQ"));
		testProts.add(new Protein(345, "AEQPGTTFELAKGQNTLVVPFVWNGPNGVSIRRTFTLERGRYAISIKDEVINKSAAPWNGYVFRKLSRVPTILSRGMTNP"));
		testProts.add(new Protein(2, "REIVGENFIIIYRLSMLDLVEGGSTLEEVIQLAKAIEKAGATIINTGIGWHEARIPTIATKVPRAAFTWVTEKLKGEVSVPLITSNRINTPEMAEHVLASGHADMVSMARPMLADPEFVLKASEGRSDEINTCIGCNQACLDHIFSMKIATCLVNPRACYETELIFKEAQNQKNIAVIGAGPAGLSFAVYAADRGHQVKIFEASHQIGGQFNIAKTVPGKEEFYETLRYFNRQIELRPNIELVLNHPATYEELSQSDFDEIVVATGVTPRQLQFEGIDHPKVLSYLQVLKERVPVGQRVAIIGAGGIGFDTAEYLTHEGESGSLNPEKFYEEWGIDTHYEHVGGLKQPKVEASEREIYLLQRKASSVGAGLGKTTGWIHRTGLKNRNVKMLAGVQYDKVDDQGLHITVDGKPTVLEVDNVVICAGQESFTAMYDQLKADGKNVHLIGGAKEAGELDAKRAIRQGAELAAVL"));
		for (Protein protein : testProts){
			protein.complete();
		}
		
		ArrayList<ArrayList<Peptide>> testPeps = Enzyme.digest(testProts);
		
		for (ArrayList<Peptide> sublist : testPeps){
			for (Peptide pep : sublist){
				System.out.println(pep.getGi() + ": " + pep.getAaSequence());
			}
		}
	}
}
