/* Copyright (c) 2014,
 * Mathias Kuhring, KuhringM@rki.de, Robert Koch Institute, Germany, 
 * All rights reserved. For details, please note the license.txt.
 */

package de.rki.ng4.lidsim;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import de.rki.ng4.lidsim.bio.Taxonomy;

public class Result {

	ArrayList<String> results;

	String filename;
	Taxonomy taxonomy;

	BufferedWriter result_bw;

	String sep = "\t";
	String header = "simlevel" + sep + "setid" + sep + "settaxid" + sep + "queryid" + sep + 
			"querygi" + sep + "querylength" + sep + "targetgi" + sep + "querytaxid" + sep + 
			"targettaxid" + sep + "lcataxid" + sep + "lcarank" + sep + 
			"querylcadist" + sep + "targetlcadist" + sep + "unique";

	public Result(String filename, Taxonomy taxonomy){
		this.filename = filename;
		this.taxonomy = taxonomy;

		try {
			result_bw = new BufferedWriter(new FileWriter(new File(filename)));
			result_bw.write(header);
			result_bw.newLine();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void close(){
		try {
			result_bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized void export(ArrayList<Match> matches, int setid, int settaxid, String simlevel){
		results = new ArrayList<String>();

		String queryid;
		int querygi, targetgi, querytaxid, querylength, targettaxid, lcataxid, querylcadist, targetlcadist;
		String lcarank;
		boolean unique;

		String tmp;
		for (Match match : matches){
			queryid = match.getQueryId();
			querygi = match.getQueryGi();
			querytaxid = match.getQueryTaxid();
			querylength = match.getQueryLength();
			targetgi = match.getTargetGi();
			targettaxid = match.getTargetTaxid();
			unique = match.getUnique();
			
			if (targetgi==0){ // no match
				lcataxid = 0;
				lcarank = "no rank";
				querylcadist = 0;
				targetlcadist = 0;
			}
			else if (targetgi==-1){ // match to genome seq with not annotation
				lcataxid = 0;
				lcarank = "no anno";
				querylcadist = 0;
				targetlcadist = 0;
			}
			else {
				lcataxid = taxonomy.getLowestCommonAncestor(querytaxid, targettaxid);
				lcarank = taxonomy.getRank(lcataxid);
				querylcadist = taxonomy.getAncestorDistance(querytaxid, lcataxid);
				targetlcadist = taxonomy.getAncestorDistance(targettaxid, lcataxid);
			}

			tmp = simlevel + "\t" + setid + "\t" + settaxid + "\t" + queryid + "\t" + 
					querygi + sep + querylength + "\t" + targetgi + "\t" + querytaxid + "\t" + 
					targettaxid + "\t" + lcataxid + "\t" + lcarank + "\t" + querylcadist + "\t" + 
					targetlcadist + sep + unique;

			try {
				result_bw.write(tmp);
				result_bw.newLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

//	public Result(ArrayList<ArrayList<Match>> matches, Taxonomy taxonomy){
//		results = new ArrayList<String>();
//
//		String simlevel = "organism";
//
//		int setid, querygi, targetgi, querytaxid, targettaxid, lcataxid, querylcadist, targetlcadist;
//		String lcarank;
//
//		String tmp;
//		for (int i=0; i<matches.size(); i++){
//			for (Match match : matches.get(i)){
//				setid = i+1;
//				querygi = match.getQueryGi();
//				querytaxid = taxonomy.giToTaxid.get(querygi);
//				targetgi = match.getTargetGi();
//
//				if (targetgi==0){
//					targettaxid = 0;
//					lcataxid = 0;
//					lcarank = "no rank";
//					querylcadist = 0;
//					targetlcadist = 0;
//				}
//				else {
//					targettaxid = taxonomy.giToTaxid.get(targetgi);
//					lcataxid = taxonomy.getLowestCommonAncestor(querygi, targetgi);
//					lcarank = taxonomy.taxidToRank.get(lcataxid);
//					querylcadist = taxonomy.getAncestorDistance(querytaxid, lcataxid);
//					targetlcadist = taxonomy.getAncestorDistance(targettaxid, lcataxid);
//				}
//
//				tmp = simlevel + "\t" + setid + "\t" + querygi + "\t" + targetgi + "\t" + querytaxid + "\t" + targettaxid + 
//						"\t" + lcataxid + "\t" + lcarank + "\t" + querylcadist + "\t" + targetlcadist;
//				results.add(tmp);
//			}
//		}
//	}
//
//	public void export(String filename){
//
//		try {
//			BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filename)));
//
//			try {
//				bw.write(header);
//				bw.newLine();
//
//				for (String result : results){
//					bw.write(result);
//					bw.newLine();
//				}
//			}
//			finally {
//				bw.close();
//			}
//
//
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
}
